# Exemplo de Tela de Checkout com Integração ao Gateway de Pagamento (3DSecure)

Este repositório contém um exemplo de tela de pagamento (checkout) utilizando a integração com o nosso gateway de pagamento, incluindo a funcionalidade de 3DSecure 2. 
Esta implementação serve como um guia prático para nossos clientes integrarem suas plataformas de e-commerce com nosso sistema de pagamento seguro utilizando o 3DS. 

## Funcionalidades Principais 

- **Integração com o Gateway de Pagamento**: A tela de checkout está configurada para se comunicar com nosso gateway de pagamento em ambiente de homologação, garantindo transações seguras e eficientes. 
- **Suporte ao 3DSecure 2.0**: Implementação do protocolo 3DSecure para autenticação adicional, proporcionando mais segurança nas transações online e reduzindo o risco de fraudes. 

## Pré-requisitos 

Antes de iniciar a implementação, certifique-se de ter as seguintes informações: 

- Conta ativa no nosso gateway de pagamento. 
- 3DSecure habilitado para a conta.
- Credenciais de API fornecidas pelo nosso gateway de pagamento (MerchatToken).

## Instalação Clone o repositório para sua máquina local: 

```
git clone https://gitlab.com/safrapay/3ds-checkout-example 
cd 3ds-checkout-example
``` 

## Configuração 

Preencha as variáveis informando o CNPJ e sua credencial de API do gateway de pagamento no arquivo *js/checkoutPayment.js*. 
```
var cnpj = "1234567000123"; 
var merchantToken = "mk_123456789";
``` 

## Executando a Aplicação 

Para iniciar a aplicação abra o arquivo checkout.html em seu navegador para visualizar a tela de checkout. 

## Estrutura do Projeto 

O projeto está estruturado da seguinte maneira: 
```
checkout.html               # Componente principal
/js 
    cardCustom.js           # Funcionalidades auxiliares
    checkoutPayment.js      # Integração com o gateway de pagamento
/css 
    checkout.css            # Estilos para a aplicação
```

## Uso da Funcionalidade 3DSecure 2.0 

A funcionalidade de 3DSecure é integrada diretamente no processo de pagamento. Durante a submissão do formulário de pagamento, a autenticação 3DSecure 2.0 é iniciada, garantindo que a transação seja autenticada pelo banco emissor do cartão. 

## Cartões de Teste

|Bandeira|Com Desafio (Challenged)|Sem Desafio (Approved)|Rejeitado|
|--------|------------------------|----------------------|---------|
|VISA|4000000000001091|4000000000001000|4000000000001018|
|MasterCard|5200000000001096|5200000000001005|5200000000001013|
|ELO|6505290000001234|6505290000001002|6505290000001010|
|AMEX|340000000001098|340000000001007|340000000001015|

## Documentação de integração

📄 https://developers.safrapay.com.br/gateway?section=3ds

## Contribuição 
Se você deseja contribuir com este projeto, sinta-se à vontade para abrir issues ou pull requests. Toda contribuição é bem-vinda!

## Licença 
Este projeto está licenciado sob a licença MIT. 
