var cnpj = "31698759001276"; // Informe o CNPJ da Loja
var merchantToken = "mk_QsnuKDEZxUuQWMN0pSaD9g"; // Informe o merchantToken fornecido pela Safrapay
var gatewayUrl = "https://payment-hml.safrapay.com.br"; // URL do gateway (homologação)
var eventOriginUrl = "https://centinelapistag.cardinalcommerce.com"; // URL de coleta (homologação)

// listener para capturar o evento enviado pelo callback
window.addEventListener('message', function(event) {
  event.data = event.data.toString();
  const data = JSON.parse(event.data);
  
  if (data && data.NewStatus) {
    if (data.NewStatus == "Approved") {
      const token = this.localStorage.getItem("token");
      const chargeId = this.localStorage.getItem("chargeId");
      pay3ds(token, chargeId);
    } else {
      Swal.fire("Rejeitado!", "Pagamento não realizado!", "error");
      $(".swal2-confirm.swal2-styled").on("click", function () {
        window.location.reload();
      });
    }
  }
});

function maskPhone(input) {
  var cleaned = input.value.replace(/\D/g, "");
  var match = cleaned.match(/^(\d{2})(\d{1,5})(\d{1,4})$/);
  if (match) {
    input.value =
      "(" + match[1] + ")" + match[2] + (match[3] ? "-" + match[3] : "");
  }
}

function maskCpfCnpj(input) {
  var cleaned = input.value.replace(/\D/g, "");
  if (cleaned.length <= 11) {
    input.value = cleaned.replace(
      /(\d{3})(\d{3})(\d{3})(\d{0,2})/,
      function (_, p1, p2, p3, p4) {
        return (
          p1 +
          "." +
          (p2 || "") +
          (p2 ? "." : "") +
          (p3 || "") +
          (p3 ? "-" : "") +
          (p4 || "")
        );
      }
    );
  } else {
    input.value = cleaned.replace(
      /(\d{2})(\d{3})(\d{3})(\d{4})(\d{0,2})/,
      function (_, p1, p2, p3, p4, p5) {
        return (
          p1 +
          "." +
          (p2 || "") +
          (p2 ? "." : "") +
          (p3 || "") +
          (p3 ? "/" : "") +
          (p4 || "") +
          (p4 ? "-" : "") +
          (p5 || "")
        );
      }
    );
  }
}

function documentDetect(input) {
  return input.length === 11 ? 1 : 2;
}

function maskCreditCard(input) {
  var value = input.value.replace(/\D/g, "");
  if (value.length > 16) {
    value = value.slice(0, 16);
  }
  var maskedValue = value.match(/.{1,4}/g)?.join(" ") || value;
  input.value = maskedValue;
}

function paymentFlow() {
  var token = generateToken();
  localStorage.setItem("token", token);
  setSetup(token);
}

function generateToken() {
  var token = "";
  var headers = {
    Authorization: merchantToken,
  };

  $.ajax({
    url: gatewayUrl + "/v2/merchant/auth",
    type: "POST",
    headers: headers,
    async: false,
    success: function (response) {
      token = "Bearer " + response.accessToken;
    },
    error: function (error) {
      console.log("Error: " + error);
    },
  });

  return token;
}

function setSetup(token) {
  var headers = {
    Authorization: token,
  };

  var setupJson = {
    transactions: [
      {
        card: {
          expirationMonth: $("#expiryDate").val().split("/")[0],
          expirationYear: $("#expiryDate").val().split("/")[1],
          number: $("#numberCard").val().replace(/\s/g, ""),
          cardholderName: $("#fullName").val(),
          cardholderDocument: $("#document").val().replace(/\D/g, ""),
          cvv: $("#cvv").val(),
          billingAddress: {
            street: $("#street").val(),
            number: $("#number").val(),
            neighborhood: $("#neighborhood").val(),
            city: $("#city").val(),
            state: $("#state").val(),
            country: "Brasil",
            zipCode: $("#zipCode").val(),
            complement: $("#complement").val(),
          },
        },
        paymentType: 2, // Tipo de pagamento 1 (Débito) e 2 (Crédito)
      },
    ],
    source: 1,
  };

  $.ajax({
    url: gatewayUrl + "/v2/charge/ecommerce/3ds/setup",
    type: "POST",
    contentType: "application/json",
    data: JSON.stringify(setupJson),
    headers: headers,
    async: false,
    success: function (response) {
      console.log("Setup");
      console.log(response);
      localStorage.setItem(
        "cardholderAuthenticationId",
        response.transactions[0].cardholderAuthenticationId
      );
      createDeviceDataCollectionListener(token, response);
      $("#jwt2").val(response.transactions[0].accessToken);
      var stepUpForm = document.querySelector("#ddc-form");
      stepUpForm.submit();
    },
    error: function (error) {
      console.log("Error: " + error);
    },
  });
}

function createDeviceDataCollectionListener(token, setupResult) {
  window.addEventListener(
    "message",
    (event) => {
      if (event.origin === eventOriginUrl) {
        let data = JSON.parse(event.data);
        if (data !== undefined && data.Status) {
          this.getEnrollment(token, setupResult);
        }
      }
    },
    false
  );
}

function getEnrollment(token, setupResult) {
  var phone = $("#phone").val().replace(/\D/g, "");

  const enrollmentJson = {
    browser: {
      httpAcceptBrowserValue: "*/*",
      httpAcceptContent: "*/*",
      userAgentBrowserValue:
        "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Mobile Safari/537.36",
      httpBrowserLanguage: "pt-BR",
      httpBrowserJavaEnabled: true,
      httpBrowserJavaScriptEnabled: true,
      httpBrowserColorDepth: "24",
      httpBrowserScreenHeight: "866",
      httpBrowserScreenWidth: "1148",
      httpBrowserTimeDifference: "180",
    },
    chargeId: setupResult.chargeId,
    cnpj: cnpj,
    customer: {
      name: $("#fullName").val(),
      email: $("#email").val(),
      documentType: documentDetect($("#document").val().replace(/\D/g, "")),
      document: $("#document").val().replace(/\D/g, ""),
      phone: {
        countryCode: "+55",
        areaCode: phone.substring(0, 2),
        number: phone.substring(2, 13),
        phoneType: 5,
      },
    },
    transactions: [
      {
        amount: 10000,
        card: {
          expirationMonth: $("#expiryDate").val().split("/")[0],
          expirationYear: $("#expiryDate").val().split("/")[1],
          number: $("#numberCard").val().replace(/\s/g, ""),
          cardholderName: $("#fullName").val(),
          cardholderDocument: $("#document").val().replace(/\D/g, ""),
          cvv: $("#cvv").val(),
          billingAddress: {
            street: $("#street").val(),
            number: $("#number").val(),
            neighborhood: $("#neighborhood").val(),
            city: $("#city").val(),
            state: $("#state").val(),
            country: "Brasil",
            zipCode: $("#zipCode").val(),
            complement: $("#complement").val(),
          },
        },
        paymentType: 2, // Tipo de pagamento 1 (Débito) e 2 (Crédito)
      },
    ],
  };
  var result = "";

  $.ajax({
    url: gatewayUrl + "/v2/charge/ecommerce/3ds/enrollment",
    type: "PUT",
    contentType: "application/json",
    data: JSON.stringify(enrollmentJson),
    beforeSend: function (xhr) {
      xhr.setRequestHeader("Authorization", token);
    },
    async: false,
    success: function (response) {
      console.log("enrollment");
      console.log(response);
      const challenge = response.transactions[0];
      const authStatus = challenge.cardholderAuthenticationStatus;

      if (authStatus === "Challenged") {
        localStorage.setItem("chargeId", response.chargeId);
        openModal(response);
        // getCharge(token);
      } else if (authStatus === "Approved") {
        pay3ds(token, response.chargeId);
      } else if (authStatus === "Rejected") {
        console.log(authStatus);
        Swal.fire("Rejeitado!", "Pagamento não realizado!", "error");
        $(".swal2-confirm.swal2-styled").on("click", function () {
          window.location.reload();
        });
      }
    },
    error: function (xhr, status, error) {
      console.log("Error: " + error);
      Swal.fire("Rejeitado!", "Pagamento não realizado!", "error");
      $(".swal2-confirm.swal2-styled").on("click", function () {
        window.location.reload();
      });
    },
  });
}

// function getCharge(token) {
//   var headers = {
//     Authorization: token,
//   };

//   var urlCharge = gatewayUrl + "/v2/charge/" + localStorage.getItem("chargeId");

//   $.ajax({
//     url: urlCharge,
//     type: "GET",
//     contentType: "application/json",
//     headers: headers,
//     async: false,
//     success: function (response) {
//       console.log("getting charge!");
//       console.log(response);
//       if (response.success) {
//         const authStatus =
//           response.charge.transactions[0].cardholderAuthentication
//             .cardholderAuthenticationStatus;
//         if (authStatus === "Approved") {
//           pay3ds(token, localStorage.getItem("chargeId"));
//         }
//       }
//     },
//     error: function (error) {
//       console.log("Error: " + error);
//     },
//   });
// }

function pay3ds(token, chargeId) {
  var headers = {
    Authorization: token,
  };

  var pay3dsJson = {
    chargeId: chargeId,
    capture: true,
    transactions: [
      {
        installmentNumber: 1, // Cobrança cartão de crédito - à vista
        installmentType: 1,
        card: {
          cvv: $("#cvv").val(),
          cardholderAuthenticationId: localStorage.getItem(
            "cardholderAuthenticationId"
          ),
        },
      },
    ],
  };

  $.ajax({
    url: gatewayUrl + "/v2/charge/ecommerce",
    type: "PUT",
    contentType: "application/json",
    data: JSON.stringify(pay3dsJson),
    headers: headers,
    async: false,
    success: function (response) {
      if (response.charge.chargeStatus === "Authorized") {
        $("#myModal").modal("hide");
        Swal.fire("Sucesso!", "Pagamento realizado com sucesso!", "success");
        $(".swal2-confirm.swal2-styled").on("click", function () {
          window.location.reload();
        });
      }
    },
    error: function (xhr, status, error) {
      console.log("Error: " + error);
      Swal.fire("Falha!", "Pagamento não realizado!", "error");
      $(".swal2-confirm.swal2-styled").on("click", function () {
        window.location.reload();
      });
    },
  });
}

function openModal(result) {
  $("#jwt").val(result.transactions[0].accessToken);
  var stepUpForm = document.querySelector("#step-up-form");
  if (stepUpForm) {
    stepUpForm.submit();
  }
  $("#myModal").modal("show");
}

function configureBillingAddress() {
  return {
    street: $("#street").val(),
    number: $("#number").val(),
    neighborhood: $("#neighborhood").val(),
    city: $("#city").val(),
    stateCode: $("#stateCode").val(),
    zipCode: $("#zipCode").val(),
    complement: $("#complement").val(),
    country: "Brasil",
  };
}

var iframe = document.getElementById("step-up-iframe");
// iframe.onload = function () {
//   setTimeout(function () {
//     var token = generateToken();
//     getCharge(token);
//   }, 5000);
// };
