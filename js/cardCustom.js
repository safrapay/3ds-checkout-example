document.addEventListener("DOMContentLoaded", function () {
  const numberCard = document.getElementById("numberCard");
  const expiryDateInput = document.getElementById("expiryDate");
  const cvv = document.getElementById("cvv");
  const cardNumber = document.querySelector(".card .numberCard");
  const cardholderName = document.getElementById("cardholderName");

  numberCard.addEventListener("input", function () {
    const cardNumberValue = this.value
      .padEnd(16, "*")
      .match(/.{1,4}/g)
      .join(" ");
    cardNumber.textContent = cardNumberValue;
  });

  cardholderName.addEventListener("input", function () {
    const fullNameValue = this.value;
    const fullNameElement = document.querySelector(".cardholderName");
    fullNameElement.textContent = fullNameValue;
  });

  expiryDateInput.addEventListener("input", function () {
    const expiryDate = this.value.replace(/[^0-9]/g, "").slice(0, 6);
    const formattedExpiryDate = expiryDate.replace(/(\d{2})(\d{2})/, "$1/$2");
    this.value = formattedExpiryDate;
    const expiryElement = document.querySelector(".expiry");
    expiryElement.textContent = formattedExpiryDate;
  });

  cvv.addEventListener("input", function () {
    const cvvValue = this.value.replace(/[^0-9]/g, "").slice(0, 3);
    const cvvElement = document.querySelector(".cvv");
    cvvElement.textContent = cvvValue;
  });
});
